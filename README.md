#README CONTEST ENTRY

# LinuxLLMImage
Hello Gitlab,

My initial submission for the hackathon consisted of a video, some pictures and a description of what I had made. As well as a description of what I would make in the future, and what tools and technologies had been used. I was advised yesterday evening European time that there needed to be code uploaded to a publicly available repository somewhere for the submission to the hackathon to be eligible.

While I have not as of this morning found the time needed to provide public code. As an alternative I now write this public repository with a description, that i am hoping perhaps can be used to satisfy the contest requirements.

In short, here is what I would do, if I was to implement things from scratch in the most scalable manner I can currently think of. 
1. **Create a custom docker image based on either Ubuntu, Linux/Webtop or Alpine Linux** depending on needs. Lets use Alpine Linux for this case, it only has console access as default but is very lightweight.
2. **Customize the Alpine image by running a docker container with it and ensuring that pip and open interpreter is installed**. Open-interpreter also requiring cargo and rustc to be installed. So, in short, install python, pip, cargo, rustc, open-interpreter in the order they are written.
3. **Create a commit of the newly customized docker container**. So that the image can be reused to create other docker  containers.
4. **Use the docker container as part of a .gitlab-ci.yml file here on Gitlab to make it usable with the CI/CD pipeline.** Ensure that the docker containers console access to the Alpine Linux inside is accessible, for example via port 8080 or similar. By having access to the console in the Alpine environment, you can now communicate with open-interpreter. When setting up this environment you will also be able to add an environment variable with any API keys to external LLMs you may want open-interpreter to use or not.
5. Since you now have Alpine Linux available with open-interpreter installed and console access, and **since it is connected to an external LLM which the open-interpreter library supports. You should in theory now have LLM access via contacting the docker image**.
6. **Open-interpreter having LLM access allows it to be controlled by the LLM**. Especially for code execution if given the flag -y, so, "interpreter -y" would allow the LLM to execute code freely within the Alpine Linux console environment. While this barebones setup does not give GUI testing, **you could also have GUI testing if you chose a Linux version with GUI**, such as Ubuntu with GNOME. Or Linux Webtop. **Please note that for open-interpreter to work with GUI related commands you would need to ensure the LLM chosen for controlling it is OpenAI and with GPT4 access via its API key**. But some console libraries, like Lynx, can interact with websites. As can likely HTTP packages and similar.
7. **Set up a webhook in for example a React project also hosted on Gitlab**, so that each time the React or other project builds. It will use the connection to the Alpine Linux environment, which then passes on that message to open-interpreter. **This message and the general instructions given to the LLM will allow it to conduct tests, fetch information and create logfiles.** I have had it do all three of these with GPT 3.5 in both a Digital Ocean droplet hosting Ubuntu Linux and a local docker environment on my laptop running Linux Webtop.

**More information:** I would probably rent GPUs on Paperspace or a similar scalable environment and then host open-source LLMs there. Mistral AI for example just released a new model that can be used, not with vision, but with creative console interpretation. Ollama could be used together with the Mistral LLM model on a Paperspace GPU instance, to be used as the LLM for open-interpreter hosted on Gitlab and elsewhere to be controlled. This LLM control via code-interpreter in Linux environments using either paid or open source LLMs, is what allows for the software testing, if given precise instructions and the LLMs potentially also being trained the right ways. GPT4 would allow for vision/graphical testing. The other LLMs would allow for console/text/code testing. I expect there will be an open source vision LLM released before too long, given the pace of development in the LLM space.

# License links

**Free software, mostly open source, including LLMs:**
- Alpine Linux license: https://github.com/alpinelinux/alpine-wiki/blob/master/LICENSE
- Ollama license: https://github.com/ollama/ollama/blob/main/LICENSE
- Open-interpreter license: https://github.com/KillianLucas/open-interpreter/blob/main/LICENSE
- Linux kernel license: https://www.kernel.org/doc/html/v4.18/process/license-rules.html
- Webtop license: https://github.com/linuxserver/docker-webtop/blob/master/LICENSE
- Ubuntu license: https://ubuntu.com/legal/open-source-licences
- Mistral license: https://github.com/openstack/mistral/blob/master/LICENSE

**Optional paid LLM and GPU services:** 
- OpenAI terms of use: https://openai.com/policies/terms-of-use
- Paperspace terms of service: https://www.paperspace.com/legal/terms-of-service

